import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const GameInfo = new Mongo.Collection('gameInfo');

GameInfo.allow({
    insert: function(userId, doc){
        // if use id exists, allow insert
        return !!userId;
    },
});

Meteor.methods({
    'add.game' (gameName, scheduledGame, gameDate, gameTime) {
        check(gameName, String);
        check(scheduledGame, Boolean);
        check(gameDate, String);
        check(gameTime, String);

        if (!this.userId) {
            throw new Meteor.Error('User is not allowed to add games. Make sure you are logged in.');
        }

        return GameInfo.insert({
            gameName: gameName,
            isSchedulted: scheduledGame,
            gameDate: gameDate,
            gameTime: gameTime,
            gameStatus: "New",
            addedOn: new Date(),
            addedBy: this.userId,
        });
    },
    'edit.game' (gameId, gameName, scheduledGame, gameDate, gameTime, gameStatus) {
        check(gameId, String);
        check(gameName, String);
        check(gameDate, String);
        check(gameTime, String);
        check(gameStatus, String);
        check(scheduledGame, Boolean);

        if (!this.userId) {
            throw new Meteor.Error('User is not allowed to edit games. Make sure you are logged in.');
        }

        return GameInfo.update({ _id: gameId }, {
            $set: {
                gameName: gameName,
                isSchedulted: scheduledGame,
                gameDate: gameDate,
                gameTime: gameTime,
                gameStatus: gameStatus,
                lastEditOn: new Date(),
                lastEditBy: this.userId,
            }
        });
    }
});