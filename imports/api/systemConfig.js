import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const SysConfig = new Mongo.Collection('sysConfig');

SysConfig.allow({
    insert: function(userId, doc){
        // if use id exists, allow insert
        return !!userId;
    },
});

Meteor.methods({
    'add.sysSettings' (allowReg, changeEmail, emailVer) {
        check(allowReg, Boolean);
        check(changeEmail, Boolean);
        check(emailVer, Boolean);

        if (!this.userId) {
            throw new Meteor.Error('User is not allowed to add system settings. Make sure you are logged in.');
        }

        let currConfig = SysConfig.findOne({});
        if (typeof currConfig != 'undefined') {
            Meteor.call('edit.sysSettings', currConfig._id,allowReg, changeEmail, emailVer, function(err, result) {
                if (err) {
                    console.log("    ERROR calling edit system settings function: " + err);
                } else {
                    console.log("Edit system settings function calld successfully");
                }
            });
        } else {
            return SysConfig.insert({
                allowReg: allowReg,
                changeEmail: changeEmail,
                emailVer: emailVer,
                dateAdded: new Date(),
            });
        }
    },
    'edit.sysSettings' (currId, allowReg, changeEmail, emailVer) {
        check(currId, String);
        check(allowReg, Boolean);
        check(changeEmail, Boolean);
        check(emailVer, Boolean);

        if (!this.userId) {
            throw new Meteor.Error('User is not allowed to edit system settings. Make sure you are logged in.');
        }

        return SysConfig.update({ _id: currId }, {
            $set: {
                allowReg: allowReg,
                changeEmail: changeEmail,
                emailVer: emailVer,
                lastEdited: new Date(),
            }
        });
    },
});