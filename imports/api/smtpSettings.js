import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const SmtpSettings = new Mongo.Collection('smtpSettings');

SmtpSettings.allow({
    insert: function(userId, doc){
        // if use id exists, allow insert
        return !!userId;
    },
});

Meteor.methods({
    'saveSMTP.settings' (host, port, emailUser, emailPass) {
        check(host, String);
        check(port, String);
        check(emailUser, String);
        check(emailPass, String);

        if (!this.userId) {
            throw new Meteor.Error('User is not allowed to add SMTP settings. Make sure you are logged in.');
        }

        let currSmtp = SmtpSettings.findOne();
        if (typeof currSmtp != 'undefined') {
            Meteor.call('updateSMTP.settings', currSmtp._id, host, port, emailUser, emailPass, function(err, result) {
                if (err) {
                    console.log("    ERROR updating SMTP settings: " + err);
                } else {
                    console.log('Update SMTP Settings function called successfully!');
                }
            });
        } else {
            return SmtpSettings.insert({
                host: host,
                port: port,
                emailUser: emailUser,
                emailPass: emailPass,
                dateAdded: new Date()
            });
        }
    },
    'updateSMTP.settings' (settingId, host, port, emailUser, emailPass) {
        check(settingId, String);
        check(host, String);
        check(port, String);
        check(emailPass, String);
        check(emailUser, String);

        if (!this.userId) {
            throw new Meteor.Error('User is not allowed to update SMTP settings. Make sure you are logged in.');
        }

        return SmtpSettings.update({ _id: settingId }, {
            $set: {
                host: host,
                port: port,
                emailUser: emailUser,
                emailPass: emailPass,
                lastEditDate: new Date()
            }
        });
    }
});