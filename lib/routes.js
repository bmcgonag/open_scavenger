FlowRouter.route('/dashboard', {
    name: 'home',
    action() {
        BlazeLayout.render('MainLayout', { main: "dashboard" });
    }
});

FlowRouter.route('/', {
    name: 'homeNoRoute',
    action() {
        BlazeLayout.render('MainLayout', { main: "home" });
    }
});

FlowRouter.route('/login', {
    name: 'login',
    action() {
        BlazeLayout.render('MainLayout', { notLoggedIn: "login" });
    }
});

FlowRouter.route('/reg', {
    name: 'reg',
    action() {
        BlazeLayout.render('MainLayout', { notLoggedIn: "reg" });
    }
});

FlowRouter.route('/userMgmt', {
    name: 'userMgmt',
    action() {
        BlazeLayout.render('MainLayout', { main: 'userMgmt' });
    }
});

FlowRouter.route('/adminInfo', {
    name: 'adminInfo',
    action() {
        BlazeLayout.render('MainLayout', { main: 'adminInfo' });
    }
});

FlowRouter.route('/systemSettings', {
    name: 'systemSettings',
    action() {
        BlazeLayout.render('MainLayout', { main: 'systemSettings' });
    }
});

FlowRouter.route('/myProfile', {
    name: 'myProfile',
    action() {
        BlazeLayout.render('MainLayout', { main: 'myProfile' });
    }
});

FlowRouter.route('/emailSetup', {
    name: 'emailSetup',
    action() {
        BlazeLayout.render('MainLayout', { main: 'emailSetup' });
    }
});

FlowRouter.route('/createGame', {
    name: 'gameWizard',
    action() {
        BlazeLayout.render('MainLayout', { main: 'gameWizard' });
    }
});