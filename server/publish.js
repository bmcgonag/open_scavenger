import { SysConfig } from '../imports/api/systemConfig.js';
import { UserConfigOptions } from '../imports/api/userConfigOptions.js';
import moment from 'moment';
import { GameInfo } from '../imports/api/gameInfo.js';
import { SmtpSettings } from '../imports/api/smtpSettings.js';

Meteor.publish("currentSettings", function() {
    try {
        return SysConfig.find({});
    } catch (error) {
        console.log("    ERROR pulling system config data: " + error);
    }
});

Meteor.publish("emailSettings", function() {
    try {
        return SmtpSettings.find({});
    } catch (error) {
        console.log("    ERROR pulling smtp email data: " + error);
    }
});

Meteor.publish('userList', function() { 
    return Meteor.users.find({});
});

Meteor.publish('myGamesMaster', function() {
    try {
        let myId = this.userId;
        return GameInfo.find({ addedBy: myId });
    } catch (error) {
        console.log("    ERROR pulling game information: " + error);
    }
});
