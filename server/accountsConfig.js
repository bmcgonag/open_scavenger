Accounts.config({
    sendVerificationEmail: true,
});

Accounts.emailTemplates.from = 'network@fixitdelrio.com';
Accounts.emailTemplates.siteName = 'Fix IT Del Rio';

Accounts.emailTemplates.verifyEmail = {
    subject() {
        return 'Confirm Your Email Address Please';
    },
    text(user, url) {
        let emailAddress = user.emails[0].address,
        urlWithoutHash = url.replace('#/', ''),
        supportEmail = "network@fixitdelrio.com",
        emailBody = "Thank you for signing up to use Open Scavenger\n\n You signed up with " + emailAddress + " . Please confirm your email address.\n\n We will not enroll you in any mailing lists, nor will we ever share your email address or personal information for any reason.\n\n You can confirm you address by clicking the following link: \n\n " + urlWithoutHash

        return emailBody;
    },
}