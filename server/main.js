import { Meteor } from 'meteor/meteor';
import { SmtpSettings } from '../imports/api/smtpSettings.js';

Meteor.startup(() => {
  // code to run on server at startup
  Roles.createRole("game-master", {unlessExists: true});
  Roles.createRole("team-captain", {unlessExists: true});
  Roles.createRole("team-member", {unlessExists: true});
  Roles.createRole("systemadmin", {unlessExists: true});

  try {
    let msgSettings = SmtpSettings.findOne({});
    if (typeof msgSettings == 'undefined' || msgSettings == null || msgSettings == "") {
      console.log("Unable to find email settings.");
    } else {
      console.log("Found email settings.");
      console.dir(msgSettings);
      let user = msgSettings.emailUser;

      let myMailUrl= 'smtp://' + encodeURIComponent(msgSettings.emailUser) + ':' + encodeURIComponent(msgSettings.emailPass) +'@' + encodeURIComponent(msgSettings.host) + ':' + msgSettings.port;
      console.log(myMailUrl);
      process.env.MAIL_URL = myMailUrl;
    }
  } catch (error) {
    console.log("    ERROR - unable to get email settings: " + error);
  }
});
