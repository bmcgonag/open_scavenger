import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';
import { Accounts } from 'meteor/accounts-base';

Meteor.methods({
    'addToRole' (userId, role) {
        let countOfUsers = Meteor.users.find().count();
        console.log("userId is: " + Meteor.userId());
        // if users = 1 - set to role passed in function call
        if (countOfUsers > 1) {
            console.log("User id for role: " + Meteor.userId() ); 
            Roles.addUsersToRoles(Meteor.userId(), role);
        } else if (countOfUsers == 1) {
            console.log("Creating first system admin user: " + Meteor.userId() );
            Roles.addUsersToRoles(Meteor.userId(), "systemadmin");
        }
    },
    'updateUser' (usersId, name, email, verified, role) {
        check(usersId, String);
        check(name, String);
        check(email, String);
        check(verified, Boolean);
        check(role, String);

        return Meteor.users.update({ _id: usersId }, {
            $set: {
                profile: {
                    fullname: name,
                },
                roles: [{
                    _id: role,
                }],
                emails: [{
                    address: email,
                    verified: verified,
                }],
            }
        });
    },
    'deleteAUser' (userId) {
        check(userId, String);

        return Meteor.users.remove({ _id: userId });
    },
    'add.newUser' (name, email, password) {
        check(name, String);
        check(email, String);
        check(password, String);
        
        Accounts.createUser({
            email: email,
            password: password,
            profile: {
                fullname: name,
            },
        });
    },
    'newUser.inRole' (userId, role) {
        check(userId, String);
        check(role, [String]);

        Roles.addUsersToRoles(userId, role);
    },
    'update.myName' (name) {
        check(name, String);

        return Meteor.users.update({ _id: this.userId }, {
            $set: {
                profile: {
                    fullname: name
                }
            }
        });
    },
    'update.myEmail' (email) {
        check(email, String);

        return Meteor.users.update({ _id: this.userId }, {
            $set: {
                'emails.0.address': email
            }
        });
    },
    'SendEmailVerificationLink' () {
        let userId = this.userId;
        console.log("the user id for email is: " + userId);
        if (userId) {
            return Accounts.sendVerificationEmail(userId);
        }
    },
});