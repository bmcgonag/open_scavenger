import { SysConfig } from "../../../imports/api/systemConfig";;

Template.login.onCreated(function() {
    this.subscribe("currentSettings");
});

Template.login.onRendered(function() {
    
});

Template.login.helpers({
    areFilled: function() {
        return Session.get("filledFields");
    },
    misEmail: function() {
        return Session.get("misEmail");
    },
    misPass: function() {
        return Session.get("misPass");
    },
    allowReg: function() {
        let sysConfig = SysConfig.findOne({});
        if (typeof sysConfig != 'undefined') {
            return sysConfig.allowReg;
        } else {
            // allow registration by default
            return true;
        }
    },
});

Template.login.events({
    'click #logmein' (event) {
        event.preventDefault();
        // console.log("clicked login");
        let email = $("#email").val();
        let pass = $("#password").val();
        let emailMiss = false;
        let passMiss = false;

        if (email == null || email == "") {
            emailMiss = true;
            Session.set("misEmail", true);
        }

        if (pass == null || pass == "") {
            passMiss = true;
            Session.set("misPass", true);
        }

        if (emailMiss == true || passMiss == true) {
            // console.log("Fields not filled");
            Session.set("filledFields", false);
        } else {
            Session.set("filledFields", true);
            Meteor.loginWithPassword(email, pass);
        }
    },
    'click #reg' (event) {
        event.preventDefault();
        FlowRouter.go('/reg');
    },
});