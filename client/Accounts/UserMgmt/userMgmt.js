Template.userMgmt.onCreated(function() {
    this.subscribe("userList");
});

Template.userMgmt.onRendered(function() {

});

Template.userMgmt.helpers({
    userInfo: function() {
        return Meteor.users.find({});
    },
    userName: function() {
        return this.profile.fullname;
    },
    userEmail: function() {
        return this.emails[0].address;
    },
    userRole: function() {
        return Roles.getRolesForUser( this._id );
    },
    isVerified: function() {
        if (this.emails[0].verified == false) {
            return "No";
        } else {
            return "Yes";
        }
    }
});

Template.userMgmt.events({
    "click .editUser" (event) {
        let userId = this._id;
        // take action
        Session.set("editUserId", userId);
        let editModal = document.getElementById('userEditModal');
        editModal.style.display = "block";
    },
    "click .deleteUser" (event) {
        event.preventDefault();
        
        let deleteId = this._id;
        // take action
        let userInfo = Meteor.users.findOne({ _id: deleteId });
        let item = userInfo.profile.fullname;
        let view = "Users";
        let method = "deleteAUser";

        Session.set("item", item);
        Session.set("view", view);
        Session.set("deleteId", deleteId);
        Session.set("method", method);
        
        let deleteModal = document.getElementById('modalDelete');
        deleteModal.style.display = 'block';
    },
    'click #addUserManually' (event) {
        event.preventDefault();
        let addUserForm = document.getElementById('addUserMan');
        addUserForm.style.display = "block";
    },
});