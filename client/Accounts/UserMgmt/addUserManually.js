
Template.addUserManually.onCreated(function() {
    this.subscribe("userList");
});

Template.addUserManually.onRendered(function() {
    Session.set("setRoles", []);
    Session.set("missingEmail", false);
    Session.set("missingName", false);
    Session.set("missingPassword", false);
    Session.set("missingPasswordConfirm", false);
    Session.set("missingReq", false);
    Session.set("noMatch", false);
});

Template.addUserManually.helpers({
    selRoles: function() {
        return Session.get("setRoles");
    },
    missingEmail: function() {
        return Session.get("missingEmail");
    },
    missingName: function() {
        return Session.get("missingName");
    },
    missingPassword: function() {
        return Session.get("missingPassword");
    },
    missingPasswordConfirm: function() {
        return Session.get("missingPasswordConfirm");
    },
    missingRequired: function() {
        return Session.get("missingReq");
    },
    noMatch: function() {
        return Session.get("noMatch");
    }
});

Template.addUserManually.events({
    'click .close' (event) {
        let addUserMan = document.getElementById('addUserMan');
        addUserMan.style.display = 'none';
    },
    'click #cancelNewUser' (event) {
        $("#fullEmail").val("");
        $("#userPass").val("");
        $("#fullUserName").val("");
        $("#userPassConf").val("");
        $("#newUserRole").val("");
        Session.set("setRoles", []);

        Session.set("missingEmail", false);
        Session.set("missingName", false);
        Session.set("missingPassword", false);
        Session.set("missingPasswordConfirm", false);
        Session.set("missingReq", false);
        Session.set("noMatch", false);

        let addUserMan = document.getElementById('addUserMan');
        addUserMan.style.display = 'none';
    },
    'click #saveNewUser' (event) {
        event.preventDefault();
        
        console.log("Clicked");
        let missingName = false;
        let missingEmail = false;
        let missingPassword = false;
        let missingPasswordConfirm = false;

        let email = $("#fullEmail").val();
        let password = $("#userPass").val();
        let name = $("#fullUserName").val();
        let confPass = $("#userPassConf").val();
        let roles = Session.get("setRoles");

        if (name == "" || name == null) {
            missingName = true;
            Session.set("missingName", true);
        }

        if (email == "" || email == null) {
            missingEmail = true;
            Session.set("missingEmail", true);
        }

        if (password == "" || password == null) {
            missingPassword = true;
            Session.set("missingPassword", true);
        }

        if (confPass == "" || confPass == null) {
            missingPasswordConfirm = true;
            Session.set("missingPasswordConfirm", true);
        }

        if (roles == null || roles == "" || roles == []) {
            roles = [team-member];
        }

        if (missingName == true || missingEmail == true || missingPassword == true || missingPasswordConfirm == true) {
            Session.set("missingReq", true);
        } else {
            Session.set("missingReq", false);
            Session.set("missingEmail", false);
            Session.set("missingName", false);
            Session.set("missingPassword", false);
            Session.set("missingPasswordConfirm", false);
            
            let userId;
            Meteor.call('add.newUser', name, email, password, function(err, result) {
                if (err) {
                    console.log("    ERROR adding user: " + err);
                } else {
                    console.log("    SUCCESS! User added.");
                    let userInfo = Meteor.users.findOne({ "emails.address": email });
                    console.log(userInfo._id);
                    userId = userInfo._id;

                    Meteor.call("newUser.inRole", userId, roles, function(err, result) {
                        if (err) {
                            console.log("    ERROR: ROLES - Error adding user to role: " + err);
                            showSnackbar("Error Adding Role to User!", "red");
                        } else {
                            console.log("User should be added to role: " + roles);
                            showSnackbar("User Added to Role!", "green");
                            $("#fullEmail").val("");
                            $("#userPass").val("");
                            $("#fullUserName").val("");
                            $("#userPassConf").val("");
                            $("#newUserRole").val("");
                        }
                    });
                }
            }); 
        }
    },
    'change #newUserRole' (event) {
        event.preventDefault();
        let roleSel = $("#newUserRole").val();
        // add to an array of roles, then send them in a helper to UI.
        let setRoles = Session.get("setRoles");
        setRoles.push(roleSel);
        Session.set("setRoles", setRoles);
    },
    'click .closeBtn' (event) {
        // get the item clicked
        let itemId = event.currentTarget.id;

        // get the current array of selected roels
        let setRoles = Session.get("setRoles");

        // remove the item clicked from the array
        setRoles.splice(setRoles.indexOf(itemId),1);

        // reset the array for display in the ui
        Session.set("setRoles", setRoles);
    },
    'focusout #userPass' (event) {
        let userPass = $("#userPass").val();
        let userPassConf = $("#userPassConf").val();
        console.log("userPassConf is: " + userPassConf);

        // first see if the confirmation has been entered
        if (userPassConf != null && userPassConf != "") {
            if (userPass != userPassConf) {
                showSnackbar("Passwords Do Not Match!", "red");
                Session.set("noMatch", true);
                return;
            } else {
                Session.set("noMatch", false);
            }
        }
    },
    'focusout #userPassConf' (event) {
        let userPass = $("#userPass").val();
        let userPassConf = $("#userPassConf").val();

        // first see if the confirmation has been entered
        if (userPass != "" && userPass != null) {
            if (userPass != userPassConf) {
                showSnackbar("Passwords Do Not Match!", "red");
                Session.set("noMatch", true);
                return;
            } else {
                Session.set("noMatch", false);
            }
        }
    }
});