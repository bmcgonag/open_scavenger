Template.userEditModal.onCreated(function() {
    this.subscribe("userList");
});

Template.userEditModal.onRendered(function() {

});

Template.userEditModal.helpers({
    userInfo: function() {
        let editUserId = Session.get("editUserId");
        return Meteor.users.find({ _id: editUserId })
    },
    fullName: function() {
        console.log("fullname is: " + this.profile.fullname);
        return this.profile.fullname;
    },
    userEmail: function() {
        return this.emails[0].address;
    },
    userRole: function() {
        let edituserId = Session.get("editUserId");
        return Roles.getRolesForUser(edituserId);
    },
    isVerified: function() {
        return this.emails[0].verified;
    },
});

Template.userEditModal.events({
    'click .saveUserInfo' (event) {
        event.preventDefault();
        let theId = Session.get("editUserId");
        let name = $("#editFullName").val();
        let email = $("#editEmail").val();
        let verif = $("#verifiedUser").prop('checked');
        let role = $("#roleName").val();
        
        
        if (role == null || role == "") {
            roleInfo = Roles.getRolesForUser(theId);
            role = roleInfo[0];
        }

        if (role == null || role == "" || name == null || name == "" || email == "" || email == null) {
            showSnackbar("All Fields Must be Filled!", "orange");
        } else {
            Meteor.call("updateUser", theId, name, email, verif, role, function(err, result) {
                if (err) {
                    console.log("    ERROR Updating the user information: " + err);
                    showSnackbar("Error Updating User Information!", "red");
                } else {
                    showSnackbar("User Information Updated Successfully!", "green");
                    let editModal = document.getElementById('userEditModal');
                    editModal.style.display = 'none';
                }
            });
        }
    },
    'click .cancelEditUser' (event) {
        let editModal = document.getElementById('userEditModal');
        editModal.style.display = 'none';
    },
    'click .close' (event) {
        let editModal = document.getElementById('userEditModal');
        editModal.style.display = 'none';
    },
});