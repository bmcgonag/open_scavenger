Template.gameWizard.onCreated(function() {

});

Template.gameWizard.onRendered(function() {
    Session.set("step", 1);
    Session.set("scheduleType", "");
    Session.set("gameNameReq", false);
    Session.set("gameInfo", {});
    Session.set("gameDate", "");
    Session.set("gameTime", "");
    Session.set("gameNoTeams", 0);
    Session.set("gameRegType", "");
});

Template.gameWizard.helpers({
    step: function() {
        return Session.get("step");
    },
    scheduleType: function() {
        return Session.get("scheduleType");
    },
    gameNameReq: function() {
        return Session.get("gameNameReq");
    },
    gameInfo: function() {
        let gameInfo = {};
        gameInfo.gameName = Session.get("gameName");
        gameInfo.gameSchedType = Session.get("scheduleType");
        if (Session.get("scheduleType") == "scheduleGame") {
            gameInfo.gameDate = Session.get("gameDate");
            gameInfo.gameTime = Session.get("gameTime");
        }
        gameInfo.gameNoTeams = Session.get("gameNoTeams");
        gameInfo.gameRegType = Session.get("gameRegType");
        return gameInfo;
    },
    noBackStep: function() {
        if ((Session.get("step")== 5) || Session.get("step") == 1) {
            return false; // don't show the back button
        } else {
            return true; // show the back button
        }
    }
});

Template.gameSummaryCard.helpers({
    gameInfo: function() {
        let gameInfo = {};
        gameInfo.gameName = Session.get("gameName");
        gameInfo.gameSchedType = Session.get("scheduleType");
        if (Session.get("scheduleType") == "scheduleGame") {
            gameInfo.gameDate = Session.get("gameDate");
            gameInfo.gameTime = Session.get("gameTime");
        }
        gameInfo.gameNoTeams = Session.get("gameNoTeams");
        gameInfo.gameRegType = Session.get("gameRegType");
        Session.set("gameInfo", gameInfo);
        return gameInfo;
    }
});

Template.gameWizard.events({
    'click #nextStep' (event) {
        event.preventDefault();
        let stepNo = Session.get("step");
        console.log("Step No: " + stepNo);
        let gameName;
        let noTeams;
        let scheduleType;
        let gameDate;
        let gameTime;
        let gameRegType;

        switch(stepNo) {
            case 1:
                gameName = $("#gameName").val();
                if (gameName == null || gameName == "") {
                    Session.set("gameNameReq", true);
                    return;
                }
                Session.set("gameName", gameName);
                increaseStep(stepNo);
                break;
            case 2:
                scheduleType = Session.get("scheduleType");
                if (scheduleType == "scheduleGame") {
                    gameDate = $("#gameDate").val();
                    gameTime = $("#gameTime").val();
                    Session.set("gameDate", gameDate);
                    Session.set("gameTime", gameTime);
                }
                increaseStep(stepNo);
                break;
            case 3:
                noTeams = $("#noTeams").val();
                Session.set("gameNoTeams", noTeams);
                increaseStep(stepNo);
                break;
            case 4:
                gameRegType = $("#regType").val();
                Session.set("gameRegType", gameRegType);
                increaseStep(stepNo);
                break;
            case 5:
                console.log("    --------------------    ");
                console.log("    Game Info:  ");
                console.log("    " + Session.get("gameName"));
                console.log("    " + Session.get("scheduleType"));
                if (Session.get("scheduleType") == "scheduleGame") {
                    console.log("    " + Session.get("gameDate"));
                    console.log("    " + Session.get("gameTime"));
                }
                console.log("    " + Session.get("gameNoTeams"));
                console.log("    " + Session.get("gameRegType"));

            default:
                break;
        }
    },
    'click #goBack' (event) {
        event.preventDefault();
        let stepNo = Session.get("step");
        Session.set("step", (stepNo - 1));
    },
    'change #schedType' (event) {
        let schedType = $("#schedType").val();
        Session.set("scheduleType", schedType);
        console.log("Schedule Type chosen: " + schedType);
    },
    'click #saveNewGaem' (event) {
        let gameInfo = Session.get("gameInfo");
        Meteor.call("saveNewGame", gameInfo.gameName, gameInfo.gameSchedType, gameInfo.gameNoTeams, gameInfo.gameRegType, function(err, result) {
            if (err) {
                showSnackbar("Error adding new game.", "red");
                console.log("")
            }
        });
    }
});

function increaseStep(stepNo) {
    stepNo == stepNo++;
    console.log("Increased step number to: " + stepNo)
    Session.set("step", stepNo);
}
