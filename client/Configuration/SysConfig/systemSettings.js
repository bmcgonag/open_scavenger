import { SysConfig } from '../../../imports/api/systemConfig.js';

Template.systemSettings.onCreated(function() {
    this.subscribe("currentSettings");
});

Template.systemSettings.onRendered(function() {
    let currSettings = SysConfig.findOne({});
    Meteor.setTimeout(function() {
        currSettings = SysConfig.findOne({});
    }, 100);
    
    Meteor.setTimeout(function() {
        $("#requireEmailVerification").prop('checked', currSettings.emailVer);
        $("#allowReg").prop('checked', currSettings.allowReg);
        $("#allowEmailChange").prop('checked', currSettings.changeEmail);
    }, 150)
    

});

Template.systemSettings.helpers({

});

Template.systemSettings.events({
    'click .saveSysSettings' (event) {
        let emailVer = $("#requireEmailVerification").prop('checked');
        let allowReg = $("#allowReg").prop('checked');
        let allowEmailChange = $("#allowEmailChange").prop('checked');
        
        Meteor.call('add.sysSettings', allowReg, allowEmailChange, emailVer, function(err, result) {
            if (err) {
                console.log("    ERROR submitting the settings changes: "+ err);
                showSnackbar("Error Saving Chages!", "red");
            } else {
                showSnackbar("Successfully Saved!", "green");
            }
        });
    },
});