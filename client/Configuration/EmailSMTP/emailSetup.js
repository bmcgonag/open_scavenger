import { SmtpSettings } from "../../../imports/api/smtpSettings";

Template.emailSetup.onCreated(function() {
    this.subscribe("emailSettings");
});

Template.emailSetup.onRendered(function() {

});

Template.emailSetup.helpers({
    smtpInfo: function() {
        return SmtpSettings.findOne({});
    },
});

Template.emailSetup.events({
    'click #saveEmailEntry'(event) {
        event.preventDefault();
        let host = $("#emailHost").val();
        let port = $("#emailPort").val();
        let emailUser = $("#emailUser").val();
        let emailPass = $("#emailPass").val();

        let hostMissing = false;
        let portMissing = false;
        let userMissing = false;
        let passMissing = false;

        // check all values
        if (host == null || host == "") {
            hostMissing = true;
        }

        if (port == null || port == "") {
            portMissing = true;
        }

        if (emailUser == null || emailUser == "") {
            userMissing = true;
        }

        if (emailPass == null || emailPass == "") {
            passMissing = true;
        }

        if (hostMissing == true || portMissing == true || userMissing == true || passMissing == true) {
            showSnackbar("All Fields are Required!", "red");
        } else {
            Meteor.call('saveSMTP.settings', host, port, emailUser, emailPass, function(err, result) {
                if (err) {
                    console.log("    ERROR saving smtp settings: " + err);
                    showSnackbar("Error Saving SMTP Settings!", "red");
                } else {
                    showSnackbar("SMTP Settings Saved Successfully!", "green");
                    $("#emailPass").val("");
                }
            });
        }
    },
    'click #cancelEmailEntry' (evnet) {
        $("#emailHost").val("");
        $("#emailPort").val("");
        $("#emailUser").val("");
        $("#emailPass").val("");
    },
});