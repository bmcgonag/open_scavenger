
Template.headerBar.onCreated(function() {

});

Template.headerBar.onRendered(function() {
    
});

Template.headerBar.helpers({
    adminReg: function() {
        return Session.get("adminreg");
    },
});

Template.headerBar.events({
	'click .navBtn' (event) {
        event.preventDefault();
        var clickedTarget = event.target.id;
        // console.log("clicked " + clickedTarget);
        if (clickedTarget == 'mainMenu') {
            FlowRouter.go('/');
        } else {
            // console.log("should be going to /" + clickedTarget);
            FlowRouter.go('/' + clickedTarget);
        }
    },
    'click .signOut': () => {
        console.log("Sign Out Clicked!");
        FlowRouter.go('/');
        Meteor.logout();
    },
    'click #brandLogo' (event) {
        event.preventDefault();
        FlowRouter.go('/dashboard');
    },
});
