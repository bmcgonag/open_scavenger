import { GameInfo } from '../../../../../imports/api/gameInfo.js';

Template.gameHome.onCreated(function() {
    this.subscribe("myGamesMaster");
});

Template.gameHome.onRendered(function() {
    $('.modal').modal();
});

Template.gameHome.helpers({

});

Template.gameHome.events({
    'click #newGame' (event) {
        // open a modal to add a new game
        $("#newGameModal").modal('open');
    },
});