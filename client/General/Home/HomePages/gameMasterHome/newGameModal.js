import { GameInfo } from "../../../../../imports/api/gameInfo";

Template.newGameModal.onCreated(function() {
    this.subscribe("myGamesMaster");
});

Template.newGameModal.onRendered(function() {
    $('.modal').modal();
    $('.datepicker').datepicker();
    $('.timepicker').timepicker();
    Session.set("showScheduler", false);
});

Template.newGameModal.helpers({
    useSched: function() {
        return Session.get("showScheduler");
    }
});

Template.newGameModal.events({
    'click #schedGame' (event) {
        if ($("#schedGame").prop('checked') == true) {
            Session.set("showScheduler", true);
        } else {
            Session.set("showScheduler", false);
        }
        Meteor.setTimeout(function() {
            $('.datepicker').datepicker();
            $('.timepicker').timepicker();
        }, 150);
    },
    'click .addNewGame' (event) {
        let gameName = $("#gameName").val();
        let isSched = $("#schedGame").prop('checked');
        let gameDate;
        let gameTime;

        if (isSched == true) {
            gameDate = $("#gameDate").val();
            gameTime = $("#gameTime").val();
        } else {
            gameDate = "No Sched";
            gameTime = "No Sched";
        }

        Meteor.call('add.game', gameName, isSched, gameDate, gameTime, function(err, result) {
            if (err) {
                console.log("    ERROR adding the game to the system: " + error);
                showSnackbar("Error Saving the Game", "red");
            } else {
                showSnackbar("Game Added Successfully!", "green");
                $("#gameName").val("");
                $("#schedGame").prop('checked', false);
                $("#gameDate").val("");
                $("#gameTime").val("");
                Session.set("showScheduler", false);
                $("#newGameModal").modal('close');
            }
        });
    }
});