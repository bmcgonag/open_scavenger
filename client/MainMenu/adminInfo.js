import { SysConfig
 } from "../../imports/api/systemConfig";
Template.adminInfo.onCreated(function() {
    this.subscribe("userList");
    this.subscribe("currentSettings");
});

Template.adminInfo.onRendered(function() {

});

Template.adminInfo.helpers({
    regUserCount: function() {
        return Meteor.users.find().count();
    },
    verUserCount: function() {
        return Meteor.users.find({ "emails.verified": true }).count();
    },
    currSettings: function() {
        return SysConfig.findOne({}); 
    },
});

Template.adminInfo.events({
    'click .route_action' (event) {
        let route = event.currentTarget.id;
        console.log("route is: " + route);
        FlowRouter.go('/' + route);
    },
});