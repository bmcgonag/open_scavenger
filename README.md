# Open Scavenger

An open source, self host-able Scavenger hunt game to help people get back out into the real world. 

## Why?
When my brother and I were young (30+ years ago), we had a person create a type of scavenger hunt that took us all over the city with an end goal of being the first team to find the great pumpkin.  Eventually my brother took the reins and created a game for a year or two as well.  We didn't have GPS, or mobile devices, so it was all just a blur of driving and running, and polaroid photos. 

I figured, why not bring it up to date a bit, and make it something people can self host, and enjoy with friend's family, or their broader community.  In reality, if this project turns out the way I'm hoping, you could run a game from anywhere in the world, with teams scattered across the globe, and still have it all be local for them.

## Work for 1st version (alpha) to be completed
- [ ] Allow a system admin to 
    - [ ] easily spin up the game in Docker
    - [X] Set the system to allow or disallow registration by users.
    - [ ] Set the system to allow a user to change his her email.
    - [ ] Set the system to force email verification or not.
    - [ ] manage users on the system.
        - [X] Edit user info.
        - [X] delete users
        - [ ] add users manually
        - [X] change user's role(s)
        - [ ] Reset a user's password (clear it and send a reset email).
        - [X] manually verify a user
    - [ ] Set and enforce a max number of simultaneous games in started state.
- [ ] A Game Master should be able to
    - [X] create a new game
    - [X] optionally schedule the game start date and time.
    - [ ] add teams to the game, or allow teams to elect to participate
    - [ ] add Routes for each team with
        - [ ] clues, waypoints, objectives, hints to be reached / completed.
    - [ ] Use an Open Street Map viewer to select locations, or 
    - [ ] use the application on their mobile device, to physically go to a location and choose to add the current location as a waypoint.
    - [ ] start a game
    - [ ] end a game
    - [ ] Send messages to team(s) via the application (in game)
    - [ ] Setup split paths based on a teams decisions in the game.
    - [ ] easily compare multiple route distances to ensure all routes are within a specified tolerance of closeness. 
    - [ ] specify the min and max number of team members required for a game.
    - [ ] Assign users as Game Controllers to help validate, and confirm or reject completion of objectives in a game by teams.
- [ ] A user should be able to sign up and be set automtically as a team-member (the first user to register is automatically the system admin).
- [ ] A user should be able to request to be made a Team Captain.
- [ ] A Team Captain should be able to approve team-members for a game.
- [ ] A user should be able to request to be part of a specific team for a specific Game.
- [ ] Team members should be able to 
    - [ ] View the game clues for the team on a mobile device when logged in to the application.
    - [ ] Submit location / clue complete confirmations during a game.
    - [ ] Send messages to other teams.
    - [ ] Send messages to their own team members (not viewable by other teams during the game).
- [ ] A game overview should be available to the Game Master and Controllers during a running game that shows
    - [ ] Map with all teams and updating locations based on traffic (color coded routes / teams / icons would be nice here - we'll see).
    - [ ] a Scoreboard showing team objective completion count.
    - [ ] Message ticker for any public messages (gm to team(s), team to team)

### Bonus Items for later
- [ ] Live camera stream from team devices in the field viewable on Game Master / Controller board.
- [ ] pinned thumbnails on map of any photo based objectives once submitted.
- [ ] Ability to bring game board up after game is ended, and view it in full, as well as filter it down by team / route by anyone that was involved with the game.
- [ ] option for a game master to propose making the game board publicly viewable (all members involved must agree).

## Contribution
If you'd like to help me make this a reality, please let me know. 

I'm using 
- MeteorJS
- Blaze front end
- JQuery / Javascript
- Milligram-CSS

#### Other things I could use help with
- Testing / QA
- Translations
- Build Pipelines in Gitlab
- Deployement (I want to use Docker)
- Development in General
- UI / UX

I'm willing to help contributors get the project running for development, and to show you a few things before pushing you to documentation. 

### License
GNU AGPL version 3
See the LICENSE.md for details.
